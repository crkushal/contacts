import React, { Component } from 'react';
import {
   View, Text, TouchableOpacity,
   Image, StyleSheet, Dimensions
} from 'react-native';

const WIDTH = Dimensions.get('window').width;

class FlatListRenderer extends Component {
   constructor(props) {
      super(props);
      this.state = {
         phoneNumber: '',
      }
   }
   componentDidMount = () => {
      this.renderPhoneNo()
   }

   renderPhoneNo() {
      const phoneNumbers = this.props.data.phoneNumbers;
      phoneNumbers.forEach(phoneNumber => {
         try {
            if (phoneNumber.constructor === Array) {
               console.log('this is an Array');
            } else {
               this.setState({
                  phoneNumber: phoneNumber.number,
               })
            }
         } catch (error) {
            console.log(error);
         }
      });
   }

   render() {
      const { data } = this.props;
      const imageSrcDefault = require('../assets/splashicon.png');
      if (this.props.index !== 0) {
         return (
            <View>
               <TouchableOpacity
                  onPress={() => this.props.navigation.navigate('SingleScreen',
                     {
                        contactDetail: data,
                        phoneNumber: this.state.phoneNumber,
                     }
                  )}
               >
                  <View style={styles.listContainer}>
                     {data.hasThumbnail === true ?
                        <Image
                           style={styles.listImage}
                           source={{ uri: data.thumbnailPath }}
                        /> :
                        <Image
                           style={styles.listImage}
                           source={imageSrcDefault}
                        />
                     }
                     <View style={styles.listTextView}>
                        {/* <Text style={styles.listText}>{this.props.index}</Text> */}
                        <Text style={styles.listText}>{data.givenName}</Text>
                        <Text style={styles.listNumber}>{this.state.phoneNumber}</Text>
                     </View>
                  </View>
               </TouchableOpacity>
            </View>
         );
      } else {
         return (
            <TouchableOpacity
               onPress={() => this.props.navigation.navigate("SingleScreen",
                  {
                     contactDetail: data,
                     phoneNumber: this.state.phoneNumber,
                  }
               )}
            >
               <View style={styles.ownerView}>
                  <Image
                     style={styles.ownerViewImage}
                     source={require('../assets/download.jpg')}
                  />
                  <View>
                     <Text style={styles.ownerViewName}>{data.givenName}</Text>
                     <Text style={styles.listNumber}>{this.state.phoneNumber}</Text>
                  </View>
               </View>
            </TouchableOpacity>
         );
      }
   }
}
const styles = StyleSheet.create({
   ownerViewName: {
      fontSize: 18,
      marginLeft: 25,
      color: 'black'
   },
   ownerViewImage: {
      width: 70,
      height: 70,
      borderRadius: 35,
   },
   ownerView: {
      flexDirection: 'row',
      borderBottomWidth: 1,
      borderColor: '#d6d7da',
      paddingHorizontal: 15,
      paddingVertical: 15
   },
   listContainer: {
      width: WIDTH,
      backgroundColor: 'white',
      flexDirection: 'row',
      marginTop: 5
   },
   listImage: {
      width: 50,
      height: 50,
      borderRadius: 25,
      marginTop: 20,
      marginLeft: 15
   },
   listText: {
      fontSize: 15,
      color: 'black',
      marginTop: 20,
      marginLeft: 25
   },
   listNumber: {
      fontSize: 18,
      color: 'black',
      marginTop: 5,
      marginLeft: 25,
   },
   listTextView: {
      flexDirection: 'column',
   }
})
export { FlatListRenderer };

