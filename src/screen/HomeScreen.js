import React, { Component } from 'react';
import {
   View, BackHandler,
   StatusBar, FlatList, Platform,
   Alert, ActivityIndicator, Animated,
   PermissionsAndroid
} from 'react-native';

import { styles } from '../styles/styles';
import { FlatListRenderer } from '../components/FlatListRenderer';
import Contacts from 'react-native-contacts';

class HomeScreen extends Component {

   static navigationOptions = {
      headerTitle: 'none',
   }

   constructor(props) {
      super(props);
      this.state = {
         text: '',
         contacts: [],
      }
      this.handleBackPress = this.handleBackPress.bind(this);
   }
   componentDidUpdate = (prevState, nextState) => {

   }
   componentWillMount = () => { this.getContacts(); }

   componentDidMount = () => {
      BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
   }

   componentWillUnmount = () => {
      BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
   }

   handleBackPress = () => {
      if (this.props.navigation.state.routeName === "HomeScreen") {
         Alert.alert(
            'Exit App',
            'Exiting the application?', [{
               text: 'Cancel',
               onPress: () => console.log("cancelable"),
               style: 'cancel'
            }, {
               text: 'OK',
               onPress: () => BackHandler.exitApp()
            },], {
               cancelable: false
            }
         )
         return true;
      } else {
         this.props.navigation.goBack();
         return true;
      }
   }

   getContacts = async () => {
      try {
         const granted = await PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.READ_CONTACTS);
         if (granted === PermissionsAndroid.RESULTS.GRANTED) {
            Contacts.getAll((err, contacts) => {
               this.setState({
                  contacts: contacts
               });
            });
         } else {
            alert("contacts permission denied")
         }
      } catch (err) {
         console.log('errro')
      }
   }

   render() {
      if (this.state.contacts.length !== 0) {
         return (
            <View style={[styles.container, { backgroundColor: 'white', }]}>
               <StatusBar
                  backgroundColor="#F4F2F5"
                  barStyle="dark-content"
               />
               <FlatList
                  data={this.state.contacts}
                  ItemSeparatorComponent={Platform.OS !== 'android' && ({ highlighted })(
                     <View style={[style.separator, highlighted && { marginLeft: 0 }]} />
                  )}
                  renderItem={({ item, index }) => {
                     return (
                        <View>
                           <FlatListRenderer data={item} index={index} navigation={this.props.navigation} />
                        </View>
                     );
                  }}
               />
            </View>
         );
      } else {
         return (
            <View>
               <ActivityIndicator size="large" color="#0000ff" />
            </View>
         );
      }
   }
}
export { HomeScreen }
