import React, { Component } from 'react';
import { View, Image, StatusBar } from 'react-native';
import { styles } from '../styles/styles';

class SplashScreen extends Component {
   static navigationOptions = {
      header: null,
   }

   navigateToHome = () => {
      setTimeout(() => {
         this.props.navigation.navigate('HomeScreen');
      }, 1000);
   }

   componentDidMount = () => {
      this.navigateToHome();
   }

   render() {
      return (
         <View style={[styles.container, { backgroundColor: '#F4F2F5' }]}>
            <StatusBar
               backgroundColor="#F4F2F5"
               barStyle="dark-content"
            />
            <Image
               source={require('../assets/splashicon.png')}
               style={styles.splashImage}
            />
         </View>
      );
   }
}
export { SplashScreen }
