import React, { Component } from 'react';
import {
   View, Text, ImageBackground,
   Dimensions, StyleSheet, ScrollView,
   TouchableOpacity, StatusBar, Button
} from 'react-native';
import Icon from 'react-native-ionicons';
import RNImmediatePhoneCall from 'react-native-immediate-phone-call';
import SmsAndroid from 'react-native-sms-android';

const WIDTH = Dimensions.get('window').width;

class SingleScreen extends Component {
   static navigationOptions = {
      header: null,
   }

   constructor(props) {
      super(props);
      this.state = {
         phoneNum: '',
         firstName: '',
         lastName: '',
         image: ''
      }
   }

   componentWillMount = () => {
      const phoneNum = this.props.navigation.state.params.phoneNumber;
      const firstName = this.props.navigation.state.params.contactDetail.givenName;
      try {
         if (isNaN(firstName)) {
            this.setState({
               phoneNum: phoneNum,
               firstName: firstName
            });
         }
      } catch (error) {
         console.log(error);
      }

   }

   immediatePhoneCall = () => {
      RNImmediatePhoneCall.immediatePhoneCall(this.state.phoneNum);
   }

   smsAndroid = () => {
      SmsAndroid.sms(
         this.state.phoneNum, // phone number to send sms to
         '', // sms body
         'sendIndirect', // sendDirect or sendIndirect
         (err, message) => {
            if (err) {
               console.log("error");
            } else {
               console.log(message); // callback message
            }
         }
      );
   }

   renderProfileImage = () => {
      const image = this.props.navigation.state.params.contactDetail;
      if (image.hasThumbnail === true) {
         return { uri: image.thumbnailPath };
      } else {
         return require('../assets/download.jpg');
      }
   }

   render = () => {
      return (
         <ScrollView>
            <StatusBar
               backgroundColor="grey"
               barStyle="light-content"
            />
            <View style={styles.singleContainer}>
               <ImageBackground
                  style={styles.singleImage}
                  source={this.renderProfileImage()}
               >
                  <View style={styles.textView}>
                     <TouchableOpacity
                        onPress={() => this.props.navigation.navigate('HomeScreen')}
                     >
                        <Icon
                           color="white"
                           size={30}
                           ios="ios-arrow-back"
                           android="md-arrow-back"
                           style={styles.arrowIcon}
                        />
                     </TouchableOpacity>
                     <Text style={styles.singleText}>
                        {this.state.firstName}
                     </Text>
                  </View>
               </ImageBackground>


               {/* for bottom texts */}
               <View style={styles.singleContet}>
                  <TouchableOpacity
                     onPress={this.immediatePhoneCall}
                  >
                     <Icon ios="ios-call" android="md-call"
                        size={30}
                        color="blue"
                        style={styles.singelContentIconFirst}
                     />
                  </TouchableOpacity>
                  <Text style={styles.singleContentText}>{this.state.phoneNum}</Text>
                  <TouchableOpacity
                     onPress={this.smsAndroid}
                  >
                     <Icon
                        ios="ios-chatbubbles"
                        android="md-chatbubbles"
                        color="blue"
                        style={styles.singelContentIconLast}
                     />

                  </TouchableOpacity>
               </View>
            </View>
         </ScrollView>
      );
   }
}
const styles = StyleSheet.create({
   arrowIcon: {
      marginLeft: 15,
      marginTop: 15
   },
   singelContentIconFirst: {
      marginLeft: 5,
      marginRight: 15,
   },
   singelContentIconLast: {
      marginRight: 30,
   },
   singleContentText: {
      fontSize: 22,
      marginRight: 110,
      color: 'black'
   },
   singleContet: {
      borderBottomWidth: 1,
      borderColor: '#d6d7da',
      justifyContent: 'space-between',
      flexDirection: 'row',
      marginTop: 5,
      paddingLeft: 20,
      paddingVertical: 15

   },
   singleContainer: {
      width: WIDTH,
   },
   singleImage: {
      width: null,
      height: 250,
      alignSelf: 'center'
   },
   textView: {
      backgroundColor: 'grey',
      width: WIDTH,
      height: 250,
      opacity: 0.5
   },
   singleText: {
      fontSize: 28,
      color: 'white',
      top: 150,
      marginLeft: 20,
      opacity: 1
   },
})
export { SingleScreen };

