import Contacts from 'react-native-contacts';
import { PermissionsAndroid } from 'react-native';

class LoadContacts {
	static requestContactsPremission = async () => {
		try {
			const granted = await PermissionsAndroid.request(
				PermissionsAndroid.PERMISSIONS.READ_CONTACTS);
			if (granted === PermissionsAndroid.RESULTS.GRANTED) {
				return Contacts.getAll((err, contacts) => {
					return contacts;
				});
			} else {
				alert("contacts permission denied")
			}
		} catch (err) {
			console.log('errro')
		}
	}
}
export { LoadContacts }
