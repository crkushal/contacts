import { createStackNavigator } from 'react-navigation';
import { SplashScreen } from '../screen/SplashScreen';
import { HomeScreen } from '../screen/HomeScreen';
import { SingleScreen } from '../screen/SingleScreen';

const AppStackNavigator = createStackNavigator({
   SplashScreen: { screen: SplashScreen },
   HomeScreen: { screen: HomeScreen },
   SingleScreen: { screen: SingleScreen }
});
export { AppStackNavigator };

