import React, { Component } from 'react';
import { AppStackNavigator } from './src/navigator/AppStackNavigator';

class App extends Component {
  render() {
    return (
      <AppStackNavigator />
    );
  }
}
export default App;

